# Java API Automation 


## Automation Structure: (en-us)

This is an automation structure for API tests with some test cases examples for each endpoint.

- Creation of Functional Tests for each endpoint (GET | POST)
- Creation of Security Tests for the endpoints that use authentication (Unauthorized | Forbidden)
- Creation of Performance Tests (multiple requests)
- Creation of Integration Tests

- Endpoint: https://dummyjson.com/#resources
- Java 8 (native)
- Maven
- Dependências
  - JUnit
  - Gson Lib
  - Surefire (reports)


## Execute the Tests

- For the execution of the tests (as a standalone), you should download the project and build with the command:

```
mvn surefire-report:report
```
- The HTML reports will be stored on
  ${ProjectFolder}/target/site

- Execute the test collection (json) with newman on the pipe -> postmancollection folder

- Execute the yml file '.gitlab-ci.yml' - located oat postmancollection folder


------

## Estrutura da Automação: (pt-br)

Esta é uma estrutura de automação de teste de API com exemplos de casos de testes pra cada endpoint.

- Criação de testes funcionais para cada endpoints (GET | POST)
- Criação de testes de segurança para os endpoint que utilizam autenticação (Unauthorized | Forbidden)
- Criação de testes de performance (envio de múltiplas requisições)
- Criação de testes de Integração

- Endpoint: https://dummyjson.com/#resources
- Java 8 (nativo)
- Maven
- Dependências
  - JUnit
  - Gson Lib
  - Surefire (reports)


## Rodar os Testes

- Para rodar os testes (standalone) basta realizar o download do projeto, realizar o build, na pasta do projeto (mesmo local onde está o arquivo pom.xml) e rodar o comando:
```
mvn surefire-report:report
```
- Os relatórios em HTML serão gerados na pasta
  ${pastaDoProjeto}/target/site