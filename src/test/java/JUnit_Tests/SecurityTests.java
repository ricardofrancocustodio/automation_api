package JUnit_Tests;

import org.junit.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SecurityTests {

    @Test
    public void testUnauthorizedAccess() throws IOException{
        URL url = new URL("https://dummyjson.com/auth/login");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Authorization", "123456789");

        // 401
        assertEquals(401, connection.getResponseCode());
    }

    @Test
    public void testNotFound_404() throws IOException  {
        URL url = new URL("https://dummyjson.com/users");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");

        //404
        assertEquals(404, connection.getResponseCode());
    }

    @Test
    public void testUsersVulnerability() throws IOException  {

        URL url = new URL("https://dummyjson.com/users");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("username", "<script>alert(1)</script>");

        //verificar codigo malicioso
        String response = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
        assertFalse(response.contains("<script>"));
    }


    @Test
    public void testSQLInjection() throws IOException {
        URL url = new URL("https://dummyjson.com/users");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("username", "' or 1 = 1 --");

        //verificar sql malicioso
        String response = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
        assertFalse(response.contains("' or 1 = 1 --"));
    }


    /**
     * AUTHLOGIN
     * */

    @Test
    public void testForbiddenAuthenticationHeader() throws IOException{
        URL url = new URL("https://dummyjson.com/auth/login");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        // 403 Forbidden
        assertEquals(403, connection.getResponseCode());
    }

    @Test
    public void testUnauthorizedAuthenticationHeader() throws IOException{
        URL url = new URL("https://dummyjson.com/auth/login");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Authorization", "999999999");

        // 401 Unauthorized
        assertEquals(401, connection.getResponseCode());
    }





}

