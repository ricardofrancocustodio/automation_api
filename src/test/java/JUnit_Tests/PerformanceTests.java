package JUnit_Tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PerformanceTests {

    @Test
    public void testServiceDenialAfterReqs() throws IOException {
        // qtdade de requisições
        int requests = 999999;
        for (int i = 0; i < requests; i++) {
            URL url = new URL("https://dummyjson.com/users");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
        }
        // ainda tá responsivo?
        URL url = new URL("https://dummyjson.com/users");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        Assert.assertEquals(200, connection.getResponseCode());
    }

    @Test
    public void testGetUsersRespTimeLessThan1000ms() throws IOException {
        URL url = new URL("https://dummyjson.com/users");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        Assert.assertEquals(200, connection.getResponseCode());
        Assert.assertTrue(connection.getResponseCode() < 1000);
    }

    @Test
    public void testGetSingleUserRespTimeLessThan1000ms() throws IOException {
        URL url = new URL("https://dummyjson.com/users/1");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        assertEquals(200, connection.getResponseCode());
        Assert.assertTrue(connection.getResponseCode() < 1000);
    }



}


