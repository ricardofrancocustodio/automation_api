package JUnit_Tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.util.List;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AuthProductsTest {

    private final AuthProducts _authProducts = new AuthProducts();

    public AuthProductsTest() throws IOException {
    }


    @BeforeAll
    public static void setupClass() {
        //System.out.println("Before All");
    }

    @BeforeEach
    public void setUp(){
        //System.out.println("Before Each");
    }


    /**
     * Test Cases
     */

    @Test
    @Order(1)
    public void testGetProductIDByTitle() {
        int productTitle_3 = _authProducts.getProductIDByTitle("Samsung Universe 9");
        int productTitle_8 = _authProducts.getProductIDByTitle("Microsoft Surface Laptop 4");
        int productTitle_16 = _authProducts.getProductIDByTitle("Hyaluronic Acid Serum");
        int productTitle_30 = _authProducts.getProductIDByTitle("Key Holder");

        Assert.assertEquals(3, productTitle_3);
        Assert.assertEquals(8, productTitle_8);
        Assert.assertEquals(16, productTitle_16);
        Assert.assertEquals(30, productTitle_30);

        //or...
        Assertions.assertEquals(3, productTitle_3);
    }

    @Test
    @Order(2)
    public void testGetProductTitleByID() {
        String productId = _authProducts.getProductTitleByID(21);
        Assert.assertEquals("- Daal Masoor 500 grams", productId);

        //or
        Assertions.assertEquals("- Daal Masoor 500 grams", productId);
    }

    @Test
    @Order(3)
    public void testGetDiscPercentById() {
        double productDiscPercent = _authProducts.getDiscPercentById(7);
        Assert.assertEquals(4.15, productDiscPercent, 0);

        //or
        Assertions.assertEquals(4.15, productDiscPercent);
    }

    @Test
    @Order(4)
    public void testGetPriceAfterDiscount() {
        double priceAfterDiscount = _authProducts.getPriceAfterDiscount("Oil Free Moisturizer 100ml");
        Assert.assertEquals(34.8, priceAfterDiscount, 0);

        //or
        Assertions.assertEquals(34.8, priceAfterDiscount);
    }

    @Test
    @Order(5)
    public void testGetProductImages() {
        List<String> productImage = _authProducts.getProductImages("Key Holder");
        for (int i = 1; i < productImage.size(); i++) {

            Assert.assertEquals("\"https://i.dummyjson.com/data/products/30/" + i + ".jpg\"", productImage.get(i - 1));

            //or
            Assertions.assertEquals("\"https://i.dummyjson.com/data/products/30/" + i + ".jpg\"", productImage.get(i - 1));
        }
    }

        /**
         * End of Test Cases
         *
         */

    @AfterEach
    public void afterEach(){
        //System.out.println("After Each");
    }

    @AfterAll
    public static void AfterAll(){
        //System.out.println("AfterAll");
    }



}
