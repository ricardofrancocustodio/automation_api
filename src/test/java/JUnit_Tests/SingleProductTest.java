package JUnit_Tests;


import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.*;

import java.io.IOException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SingleProductTest {

    SingleProduct _singleProduct = new SingleProduct(99);

    public SingleProductTest() throws IOException {
    }

    @BeforeAll
    public static void setupClass(){
        //System.out.println("Before All");
    }

    @BeforeEach
    public void setUp(){
        //System.out.println("Before Each");
    }


    /**
     * Test Cases
     */

    @Test
    @Order(1)
    public void testGetBrandByTitleMethod() {
        Assert.assertEquals(_singleProduct.getBrand(), _singleProduct.getBrandByTitle(_singleProduct.getTitle()));

        //or...
        Assertions.assertEquals(_singleProduct.getBrand(), _singleProduct.getBrandByTitle(_singleProduct.getTitle()));
    }


    //getRatingByID
    @Test
    @Order(2)
    public void testGetRatingByIDMethod() {
        Assert.assertEquals(_singleProduct.getRating(), _singleProduct.getRatingByID(_singleProduct.getId()), 0);

        //or...
        Assertions.assertEquals(_singleProduct.getRating(), _singleProduct.getRatingByID(_singleProduct.getId()), 0);
    }

    //getImagesOfProductByTitle
    @Test
    @Order(3)
    public void testGetImagesOfProductByTitle() {
        Assert.assertEquals(_singleProduct.getImages(), _singleProduct.getImagesOfProductByTitle(_singleProduct.getTitle()));

        //or...
        Assertions.assertEquals(_singleProduct.getRating(), _singleProduct.getRatingByID(_singleProduct.getId()), 0);
    }

    //
}
