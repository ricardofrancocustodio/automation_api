package JUnit_Tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IntegrationTest {

    @Test
    public void testCheckAddProductsObj() throws IOException {
        // Given
        AddProducts addProducts = new AddProducts("https://dummyjson.com/products/add");

        // When
        int statusResponse = addProducts.get_statusResponse();

        // Then
        Assertions.assertEquals(201, statusResponse);
    }


    @Test
    public void testCheckAuthLoginObj() throws IOException {
        // Given
        AuthLogin authLogin =  new AuthLogin();

        // When
        String content_type = authLogin.checkHTTPHeader();
        boolean bodyOk = authLogin.checkHTTPBody();

        // Then
        Assertions.assertNotNull(content_type);
        Assertions.assertTrue(bodyOk);
        //criar para todos os métodos...
    }


}
