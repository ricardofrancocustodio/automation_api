package JUnit_Tests;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SingleProduct {

    JsonObject _singleProductObj;
    String _title;
    String _brand;
    double _rating;
    int _id;
    JsonArray _images;

    public SingleProduct(int id) throws IOException {

        URL url = new URL("https://dummyjson.com/products/"+ id);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        // Ler resposta JSON do endpoint
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            response.append(line);
        }
        reader.close();

        Gson gson = new Gson();
        this._singleProductObj = gson.fromJson(response.toString(), JsonObject.class) ;
        this._id = gson.fromJson(response.toString(), JsonObject.class).get("id").getAsInt();
        this._title = gson.fromJson(response.toString(), JsonObject.class).get("title").getAsString();
        this._brand = gson.fromJson(response.toString(), JsonObject.class).get("brand").getAsString();
        this._rating = gson.fromJson(response.toString(), JsonObject.class).get("rating").getAsDouble();
        //parse
        this._images = JsonParser.parseString(response.toString()).getAsJsonObject().getAsJsonArray("images");

    }

    public String getTitle(){
        return _title;
    }

    public double getRating(){
        return _rating;
    }

    public int getId(){
        return _id;
    }

    public String getBrand(){
        return _brand;
    }

    public JsonArray getImages(){
        return _images;
    }

    public String getBrandByTitle(String title){
        return _title.equals(title) ? _brand : null;
    }

    public double getRatingByID(int id){
        return _id == id ? _rating : 0;

    }

    public JsonArray getImagesOfProductByTitle(String title){
        return _title.equals(title) ? _images : null;
    }


    @Override
    public String toString() {
        return "SingleProduct"  + _singleProductObj ;
    }
}
