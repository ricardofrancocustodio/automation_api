package JUnit_Tests;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AuthProducts {

    private JsonArray productsArray = null;

    public AuthProducts() throws IOException {

        JsonObject credentials = new JsonObject();
        credentials.addProperty("username", "kminchelle");
        credentials.addProperty("password", "0lelplR");

        String request = new Gson().toJson(credentials);

        URL urlLogin = new URL("https://dummyjson.com/auth/login");
        HttpURLConnection connectionLogin = (HttpURLConnection) urlLogin.openConnection();
        connectionLogin.setRequestMethod("POST");
        connectionLogin.setRequestProperty("Content-Type", "application/json");
        connectionLogin.setDoOutput(true);

        OutputStream outputStream = connectionLogin.getOutputStream();
        outputStream.write(request.getBytes());
        outputStream.flush();
        outputStream.close();

        if(connectionLogin.getResponseCode() == 200){

            BufferedReader reader = new BufferedReader(new InputStreamReader(connectionLogin.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            //reader.close();

            JsonObject responseJson = new Gson().fromJson(response.toString(), JsonObject.class);
            URL url = new URL("https://dummyjson.com/auth/products");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", "Bearer " + responseJson.get("token").getAsString());
            connection.setDoOutput(true);

            BufferedReader reader_ = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response_ = new StringBuilder();
            String line_;

            while ((line_ = reader_.readLine()) != null) {
                response_.append(line_);
            }

            reader_.close();

            this.productsArray = JsonParser.parseString(response_.toString()).getAsJsonObject().getAsJsonArray("products");

        }

    }

    //get product ID of by title
    public int getProductIDByTitle(String title) {
        int id = 0;

        for (JsonElement product : productsArray) {
            JsonObject productObject = product.getAsJsonObject();

            if(productObject.get("title").getAsString().equals(title)){
                id = productObject.get("id").getAsInt();
            }
        }

        return id;
    }


    //get product title by ID
    public String getProductTitleByID(int id) {
        String prod = "";

        for (JsonElement product : productsArray) {
            JsonObject productObject = product.getAsJsonObject();

            if(productObject.get("id").getAsInt() == id){
                prod = productObject.get("title").getAsString();
            }
        }

        return prod;
    }


    //get discount percentage by ID
    public double getDiscPercentById(int id) {
        double discountPercent = 0.0;

        for (JsonElement product : productsArray) {
            JsonObject productObject = product.getAsJsonObject();

            if(productObject.get("id").getAsInt() == id){
                discountPercent = productObject.get("discountPercentage").getAsDouble();
            }
        }

        return discountPercent;
    }


    //get price after discount percentage
    public double getPriceAfterDiscount(String title){
        double finalPrice = 0.0;

        for (JsonElement product : productsArray) {
            JsonObject productObject = product.getAsJsonObject();
            double price = productObject.get("price").getAsDouble();
            double discountPercentage = productObject.get("discountPercentage").getAsDouble();

            if(productObject.get("title").getAsString().equals(title)){

                double prodPart = price / 100;
                finalPrice = price - (prodPart * Math.round(discountPercentage));
            }
        }

        return finalPrice;
    }


    //get product images
    public List<String> getProductImages(String title) {
        List<String> productImages = new ArrayList<>();

        for (JsonElement product : productsArray) {
            JsonObject productObject = product.getAsJsonObject();

            if(productObject.get("title").getAsString().equals(title)){
                JsonArray images = productObject.get("images").getAsJsonArray();

                for (JsonElement img : images) {
                    productImages.add(img.toString());
                }
            }
        }
        return productImages;
    }

}
